<?php

require_once 'Alumno.php';

//Implementar aquí la clase AlumnoPresencial
class AlumnoPresencial extends Alumno {
    private $asistencia;

    public function __construct($nombre, $apellido, $dni, $porcentaje, $nota)
    {
        parent::__construct($nombre, $apellido, $dni, $porcentaje);
        $this->notaExamenFinal = $nota;
        $this->porcentajeAsistencia = $porcentaje;
    }

    public function getNota()
    {
        if ($this->porcentajeAsistencia < 70) {
            $nota_final = 1;
            return $nota_final;
        } else {
            if (count($this->notaExamenFinal) === 1) {
                $nota_final = $this->notaExamenFinal[0];
                return $nota_final;
            } else {
                $nota_mas_baja = 10;
                $cantidad_de_notas = count($this->notaExamenFinal) - 1;
                $suma_notas = 0;
                foreach ($this->notaExamenFinal as $key => $value) {
                    if ($value < 6) {
                        $nota_final = 1;
                        return $nota_final;
                    } 
                    if ($value < $nota_mas_baja) {
                        $nota_mas_baja =  $value;
                    }
                    $suma_notas = $suma_notas + $value;
                }
                $promedio = ($suma_notas - $nota_mas_baja) / $cantidad_de_notas;
                return $promedio;
            }
        }
    }
}