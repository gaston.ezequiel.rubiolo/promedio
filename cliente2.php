<?php
require_once 'AlumnoLibre.php';
require_once 'AlumnoPresencial.php';

$fulano = new AlumnoLibre('Fulano', 'de Tal', 33333333, 7);
echo "Nombre, apellido, DNI, y nota del alumno: " . $fulano;

echo '<br>';

$notasDeMengano = [ 9, 7, 8 ];
$porcentajeAsistencia = 90;
$mengano = new AlumnoPresencial(
    'Mengano', 'de Cual', 34444444, $porcentajeAsistencia, $notasDeMengano
);
echo "Nombre, apellido, y nota del alumno: " . $mengano;

