# Consigna entrevista mayo 2024

En un curso se admiten dos tipos de alumno: libre y presencial.

- De todos los alumnos se tiene: nombre, apellido, y DNI.
- De los alumnos presenciales, se tiene un array de notas que corresponden a las 
notas de los TP (no se sabe cuántos TP habrá). Se tiene también un porcentaje de asistencia.
- De los alumnos libres, se tiene una nota de examen final.

## Cálculo de nota final

1. La nota final de los alumnos libres es la nota del examen final.
2. La nota final de los alumnos presenciales, es la siguiente: 
    - Si el porcentaje de asistencia es menor a 70%, la nota final es 1(uno). 
    - Si hay algún TP con nota menor a 6(seis), la nota final es 1(uno).
    - Si no ocurre ninguno de los dos casos anteriores, la nota final se calcula así:
        * Si el array con notas de los TP tiene un solo elemento, la nota final es ese valor.
        * Si el array con notas de los TP tiene más de un elemento, la nota final es 
        el promedio de los TP, sin tener en cuenta la nota más baja.    

## Se solicita:

1. Implementar la clase `AlumnoPresencial`, en el archivo `AlumnoPresencial.php`.
El porcentaje mínimo de asistencia es 70%, pero podría variar en un futuro.
2. Si se intenta convertir en cadena (string) a un objeto `AlumnoLibre` o 
`AlumnoPresencial`, se debe mostrar nombre, apellido y nota.
Se puede probar el funcionamiento con el archivo `cliente2.php`.
3. Asegurarse de que todas las clases que hereden de `Alumno` (incluso clases 
futuras, aún no implementadas) tendrán implementado un método llamado `getNota()`. 

Para comprender mejor el punto 1), van varios ejemplos para alumnos presenciales:

EJEMPLO 1:
Porcentaje de asistencia: `60`
Notas de los TP: `9 - 7 - 8`
Nota final: `1 `
(porque no llega al 70% de asistencia)

EJEMPLO 2:
Porcentaje de asistencia: `80%`
Notas de los TP: `9 - 5 - 8`
Nota final: `1 `
(porque hay un TP con nota menor que 6)

EJEMPLO 3:
Porcentaje de asistencia: `80%`
Notas de los TP: `8`
Nota final: `8 `
(como hay un solo TP, la nota final es la nota de ese TP)

EJEMPLO 4:
Porcentaje de asistencia: `80%`
Notas de los TP: `8 - 6 - 7 - 6`
Nota final: `7 `
(Se elimina la peor de las notas: uno de los "6".
Luego se calcula el promedio: `(8 + 6 + 7) / 3 = 7`

