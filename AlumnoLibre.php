<?php
require_once 'Alumno.php';

class AlumnoLibre extends Alumno
{
    private $notaExamenFinal;

    public function __construct($nombre, $apellido, $dni, $nota)
    {
        parent::__construct($nombre, $apellido, $dni);
        $this->notaExamenFinal = $nota;
    }

    public function getNota()
    {
        return $this->notaExamenFinal;
    }
}
