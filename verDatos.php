<?php
require_once 'AlumnoPresencial.php';
require_once 'AlumnoLibre.php';

if($_POST['tipo'] == 'libre') {
    $alumno = new AlumnoLibre($_POST['nombre'], $_POST['apellido'], $_POST['dni'], $_POST['notaFinal']);
}
elseif ($_POST['tipo'] == 'presencial') {
    $alumno = new AlumnoPresencial($_POST['nombre'], $_POST['apellido'], $_POST['dni'], $_POST['porcentaje'], $_POST['notas']);
}

$nombre = $alumno->getNombreApellido();
$dni = $alumno->getDni();
$nota = $alumno->getNota();

echo "Estudiante $nombre - DNI: $dni - Nota: $nota";
